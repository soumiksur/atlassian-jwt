package com.atlassian.jwt.internal;

import com.atlassian.jwt.JwtIssuer;
import com.atlassian.jwt.JwtIssuerClaimVerifiersRegistry;
import com.atlassian.jwt.JwtIssuerRegistry;
import com.atlassian.jwt.core.reader.JwtIssuerSharedSecretService;
import com.atlassian.jwt.core.reader.JwtIssuerValidator;
import com.atlassian.jwt.exception.JwtIssuerLacksSharedSecretException;
import com.atlassian.jwt.exception.JwtUnknownIssuerException;
import com.atlassian.jwt.reader.JwtClaimVerifiersBuilder;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class PluginJwtRegistry implements JwtIssuerClaimVerifiersRegistry, JwtIssuerRegistry,
        JwtIssuerSharedSecretService, JwtIssuerValidator
{
    private final ServiceTracker claimsVerifierServiceTracker;
    private final ServiceTracker issuerServiceTracker;

    public PluginJwtRegistry(BundleContext bundleContext)
    {
        claimsVerifierServiceTracker = new ServiceTracker(bundleContext, JwtIssuerClaimVerifiersRegistry.class.getName(), null);
        claimsVerifierServiceTracker.open();
        issuerServiceTracker = new ServiceTracker(bundleContext, JwtIssuerRegistry.class.getName(), null);
        issuerServiceTracker.open();
    }

    public void destroy()
    {
        claimsVerifierServiceTracker.close();
        issuerServiceTracker.close();
    }

    @Override
    public JwtClaimVerifiersBuilder getClaimVerifiersBuilder(@Nonnull String issuerName)
    {
        requireNonNull(issuerName, "issuerName");

        for (JwtIssuerClaimVerifiersRegistry registry : getClaimVerifiersRegistries())
        {
            JwtClaimVerifiersBuilder claimVerifiersBuilder = registry.getClaimVerifiersBuilder(issuerName);
            if (claimVerifiersBuilder != null)
            {
                return claimVerifiersBuilder;
            }
        }
        return null;
    }

    @Override
    public JwtIssuer getIssuer(@Nonnull String issuerName)
    {
        requireNonNull(issuerName, "issuerName");

        for (JwtIssuerRegistry registry : getIssuerRegistries())
        {
            JwtIssuer issuer = registry.getIssuer(issuerName);
            if (issuer != null)
            {
                return issuer;
            }
        }
        return null;
    }

    @Override
    public String getSharedSecret(@Nonnull String issuerName) throws JwtIssuerLacksSharedSecretException, JwtUnknownIssuerException
    {
        String secret = getIssuerOrFail(issuerName).getSharedSecret();
        if (null == secret)
        {
            throw new JwtIssuerLacksSharedSecretException(issuerName);
        }

        return secret;
    }

    @Override
    public boolean isValid(String issuerName)
    {
        return issuerName != null && getIssuer(issuerName) != null;
    }

    private Iterable<JwtIssuerClaimVerifiersRegistry> getClaimVerifiersRegistries()
    {
        return getRegistries(claimsVerifierServiceTracker, JwtIssuerClaimVerifiersRegistry.class);
    }

    private JwtIssuer getIssuerOrFail(String issuerName) throws JwtUnknownIssuerException
    {
        JwtIssuer issuer = getIssuer(issuerName);
        if (issuer == null)
        {
            throw new JwtUnknownIssuerException(String.format("Issuer '%s' not found", issuerName));
        }
        return issuer;
    }

    private Iterable<JwtIssuerRegistry> getIssuerRegistries()
    {
        return getRegistries(issuerServiceTracker, JwtIssuerRegistry.class);
    }

    private <T> Iterable<T> getRegistries(ServiceTracker serviceTracker, Class<T> clazz)
    {
        List<T> registries = new ArrayList<>();
        Object[] services = serviceTracker.getServices();
        if (services != null)
        {
            for (Object service : services)
            {
                if (clazz.isInstance(service))
                {
                    registries.add(clazz.cast(service));
                }
            }
        }

        return registries;
    }
}
