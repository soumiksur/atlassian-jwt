package it;

import com.atlassian.jwt.server.JwtPeer;
import com.atlassian.jwt.server.servlet.JwtRegistrationServlet;
import com.atlassian.jwt.util.HttpUtil;
import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static it.util.HttpResponseConsumers.expectStatus;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;

/**
 *
 */
public abstract class AbstractPeerTest
{

    protected String baseUrl;
    private String contextPath;

    public AbstractPeerTest()
    {
        initializeBaseUrlFromSystemProperty();
    }

    protected String getContextPath()
    {
        return contextPath;
    }

    protected String jwtTestBasePath()
    {
        return baseUrl + "/rest/jwt-test/latest";
    }

    protected String registrationResource()
    {
        return jwtTestBasePath() + "/register";
    }

    protected String whoAmIResource()
    {
        return jwtTestBasePath() + "/whoami";
    }

    protected String relayResource(String id)
    {
        return jwtTestBasePath() + "/relay/" + id;
    }

    public void registerPeer(JwtPeer peer) throws IOException
    {
        HttpUtil.post(registrationResource(), ImmutableMap.of(
                "baseUrl", peer.getBaseUrl(),
                "path", JwtRegistrationServlet.PATH
        ), expectStatus(SC_OK));
    }

    public void unregisterPeer(JwtPeer peer) throws IOException
    {
        HttpUtil.delete(registrationResource() + "/" + peer.getSecretStore().getClientId(), expectStatus(SC_NO_CONTENT));
    }

    /**
     * Return the base URL for the current running product, based on available system properties.
     *
     * @return the product base URL
     * @see TestedProductFactory#create(Class, String, TestedProductFactory.TesterFactory)
     */
    private void initializeBaseUrlFromSystemProperty() {
        Class<? extends TestedProduct> testedProductClass = getTestedProductClass();
        Defaults defaults = testedProductClass.getAnnotation(Defaults.class);
        baseUrl = Optional.ofNullable(System.getProperty("baseurl." + defaults.instanceId()))
                .orElseGet(() -> "http://localhost:" + defaults.httpPort() + defaults.contextPath());
        try {
            contextPath = new URI(baseUrl).getPath();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static Class<? extends TestedProduct> getTestedProductClass() {
        String testedProductClassName = System.getProperty("testedProductClass");
        try {
            return (Class<? extends TestedProduct>) Class.forName(testedProductClassName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
